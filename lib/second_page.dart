import 'package:flutter/material.dart';
import 'main.dart';

class SecondPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF7F3F0),
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: kGreyColor,
        title: Text(
          'חשבון חדש',
        ),
        actions: <Widget>[
          Icon(
            Icons.clear,
            size: 30.0,
          ),
          SizedBox(
            width: 50.0,
          ),
        ],
      ),
      bottomNavigationBar: BottomAppBar(
        color: Color(
          0xFFFFD001,
        ),
        child: FlatButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => MyDialog()),
            );
          },
          child: Icon(
            Icons.done,
            size: 90.0,
            color: Colors.white,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.only(left: 10.0, right: 10.0),
          child: Directionality(
            textDirection: TextDirection.rtl,
            child: Column(
              children: <Widget>[
                ReusableTextField(
                  hinttext: 'אימייל',
                  smallIcon: Icons.mail,
                ),
                ReusableTextField(
                  hinttext: 'אימות אימייל',
                  smallIcon: Icons.mail,
                ),
                ReusableTextField(
                  hinttext: 'סיסמא',
                  smallIcon: Icons.vpn_key,
                ),
                ReusableTextField(
                  hinttext: 'אימות סיסמא',
                  smallIcon: Icons.vpn_key,
                ),
                CheckBoxRow(
                  checktext: 'קראתי את התנאים',
                ),
                CheckBoxRow(
                  checktext: 'השאר אותי מחובר  לחשבון',
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
