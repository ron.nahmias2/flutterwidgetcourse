import 'package:design_page_app/second_page.dart';
import 'package:flutter/material.dart';
import 'main.dart';

class FirstPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF7F3F0),
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: kGreyColor,
        title: Text(
          'חשבון חדש',
        ),
        actions: <Widget>[
          Icon(
            Icons.clear,
            size: 30.0,
          ),
          SizedBox(
            width: 50.0,
          ),
        ],
      ),
      bottomNavigationBar: BottomAppBar(
        color: Color(
          0xFFFFD001,
        ),
        child: FlatButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => SecondPage()),
            );
          },
          child: Icon(
            Icons.arrow_forward_ios,
            size: 90.0,
            color: Colors.white,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.only(left: 10.0, right: 10.0),
          child: Directionality(
            textDirection: TextDirection.rtl,
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  width: double.infinity,
                  height: 200.0,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.black,
                    ),
                  ),
                  child: Icon(
                    Icons.camera_enhance,
                  ),
                ),
                ReusableTextField(
                  hinttext: 'שם פרטי',
                  smallIcon: Icons.person,
                ),
                ReusableTextField(
                  hinttext: 'שם משפחה',
                  smallIcon: Icons.person,
                ),
                ReusableTextField(
                  hinttext: 'תאריך לידה',
                  smallIcon: Icons.calendar_today,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
