import 'package:circular_check_box/circular_check_box.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'second_page.dart';
import 'first_page.dart';
import 'package:fid_widgets/fid_widgets.dart';

const Color kGreyColor = Color(0xFFADA49B); // grey color

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Widgets Layout App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: SafeArea(
        child: FirstPage(),
      ),
    );
  }
}

class CheckBoxRow extends StatefulWidget {
  final String checktext;
  CheckBoxRow({this.checktext});
  @override
  _CheckBoxRowState createState() => _CheckBoxRowState();
}

class _CheckBoxRowState extends State<CheckBoxRow> {
  bool checkboxval = false;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(widget.checktext),
        CircularCheckBox(
          value: checkboxval,
          activeColor: Color(
            0xFFFFD001,
          ),
          materialTapTargetSize: MaterialTapTargetSize.padded,
          onChanged: (bool x) {
            setState(() {
              checkboxval = !checkboxval;
            });
          },
        ),
      ],
    );
  }
}

class ReusableTextField extends StatelessWidget {
  final String hinttext;
  final IconData smallIcon;
  ReusableTextField({@required this.hinttext, @required this.smallIcon});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10.0),
      child: Container(
        color: Colors.white,
        child: TitledBoarderFormField(
          icon: smallIcon,
          title: hinttext,
        ),
      ),
    );
  }
}

class MyDialog extends StatefulWidget {
  @override
  _MyDialogState createState() => _MyDialogState();
}

class _MyDialogState extends State<MyDialog> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: kGreyColor,
      title: Text(
        'החשבון נוצר',
        style: TextStyle(color: Colors.white),
      ),
      content: Text(
        'לחץ על המשך להתחברות',
        style: TextStyle(color: Colors.white),
      ),
      actions: <Widget>[
        FlatButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Center(
            child: Text('המשך',style: TextStyle(color: Colors.white),),
          ),
        ),
      ],
    );
  }
}
